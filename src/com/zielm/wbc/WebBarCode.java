package com.zielm.wbc;

import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.Toast;
import android.widget.TextView;
import java.security.SecureRandom;
import java.math.BigInteger;
import java.net.*;
import java.io.*;
import com.google.zxing.integration.android.*;

public class WebBarCode extends Activity
{
    public final String BASE_URL = "http://qr.zielm.com/";
    private String url;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        System.err.println("WebBarCode start");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        setupURL();

        ((TextView)findViewById(R.id.url)).setText(url);
        findViewById(R.id.scanbutton).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    IntentIntegrator integrator = new IntentIntegrator(WebBarCode.this);
                    integrator.initiateScan();
                }
            });
    }

    private void setupURL() {
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        String id = preferences.getString("webid", null);
        if(id == null) {
            SharedPreferences.Editor editor = preferences.edit();
            id = new BigInteger(40, new SecureRandom()).toString(32);
            editor.putString("webid", id);
            editor.commit();
        }
        url = BASE_URL + id;
    }

    private static String pickleResult(IntentResult result) {
        return "content=" + encode(result.getContents()) +
            "&formatName=" + encode(result.getFormatName()) +
            "&errorCorrectionLevel=" + encode(result.getErrorCorrectionLevel());
    }

    private static String encode(String text) {
        try {
            return URLEncoder.encode(text == null ? "" : text, "UTF-8");
        } catch(java.io.UnsupportedEncodingException ex) {
            throw new Error("checked exceptions!");
        }
    }

    private void sendResult(String texturl) {
        try {
            System.err.println("fetch " + texturl);
            URL url = new URL(texturl);

            BufferedReader in =
                new BufferedReader(new InputStreamReader(url.openStream()));
            final String result = in.readLine();
            if(result != "ok")
                throw new IOException("bad result: " + result);
            in.close();
            runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(WebBarCode.this,
                                       "refresh page to see your code", Toast.LENGTH_LONG);
                    }
                });
        } catch(final IOException ex) {
            runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(WebBarCode.this, "error: " + ex, Toast.LENGTH_LONG);
                    }
                });
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanResult != null) {
            final String result = pickleResult(scanResult);
            Toast.makeText(this, "sending result...", Toast.LENGTH_LONG);
            (new Thread() {
                public void run() {
                    sendResult(url + "/post?" + result);
                }
                }).start();
        }
    }
}
